# Hey there, I'm Jiaan 👋

My name is Jiaan Louw, pronounced _yee-aan low_, and I'm a Software Engineer at GitLab.

- [LinkedIn](https://www.linkedin.com/in/jiaan-louw/)
- [Personal website](https://jiaanlouw.com/)

# How I work

- My timezone is GMT+2.
- I enjoy and prefer working asynchronously.
- I always try to be kind even when I'm being direct.
- I try to consolidate meetings into my mornings and late afternoons.
- I am always open for a social coffee chat. ☕

# How to work with me

- I prefer async and transparent communication on GitLab. If you need me urgenntly ping me on Slack.
- I enjoy working outside of frotend tasks so feel free to ping me on product, design or backend issues.
